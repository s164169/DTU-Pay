package com.dtupay.monolith.Adapters.Interfaces;


import com.dtupay.monolith.DAL.Models.DTUPayAccount;
import com.dtupay.monolith.DAL.Models.DTUPayAccountInfo;
import dtupay.core.DAL.models.DTUPayUser;

import java.math.BigDecimal;

public interface IBank {

    public String createAccountWithBalance(DTUPayUser dtuPayUser, BigDecimal initialBalance) throws Exception;

    public DTUPayAccount getAccount(String accountNumber) throws Exception;

    public DTUPayAccount getAccountByCPRNumber(String cpr) throws Exception;

    public DTUPayAccountInfo[] getAccounts() throws Exception;

    public void retireAccount(String accountNumber) throws Exception;

    public void transferMoneyFromTo(String accountFrom, String accountTo, BigDecimal amount, String description) throws Exception;

}
