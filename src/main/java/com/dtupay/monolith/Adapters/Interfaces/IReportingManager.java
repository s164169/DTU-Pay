package com.dtupay.monolith.Adapters.Interfaces;

import com.dtupay.monolith.Models.Report;
import dtupay.core.DAL.models.Merchant;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface IReportingManager {
    List<Report> getReportByMerchantID(UUID merchantID, LocalDate start, LocalDate end) throws Exception;

    List<Report> getReportByCustomerID(UUID customerID, LocalDate start, LocalDate end) throws Exception;

    void createMerchant(Merchant merchant) throws Exception;
}
