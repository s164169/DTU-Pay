package com.dtupay.monolith.Adapters.Interfaces;

import dtupay.core.DAL.models.Token;

import java.util.List;
import java.util.UUID;

public interface ITokenManager {

    int getCustomersNumberOfTokens(UUID customerID) throws Exception;

    List<Token> requestTokens(UUID customerID, int requestedNumberOfTokens) throws Exception;

    boolean validateToken(Token token) throws Exception;

}
