package com.dtupay.monolith.Adapters.Interfaces;

import dtupay.core.models.DTOs.PaymentDTO;
import dtupay.core.models.DTOs.TransferMoneyDTO;

public interface ITransactionManager {
    void transferMoney(PaymentDTO paymentDTO) throws Exception;

    void refundPayment(PaymentDTO paymentDTO) throws Exception;

}
