package com.dtupay.monolith.Adapters;

import com.dtupay.monolith.DAL.InMemoryRepository;
import com.dtupay.monolith.Models.*;
import com.dtupay.monolith.DTUPay;
import com.dtupay.monolith.Models.DTUPayUserDTO;
import com.google.gson.Gson;
import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.DAL.models.Token;
import dtupay.core.models.DTOs.PaymentDTO;
import dtupay.core.models.DTOs.TransferMoneyDTO;

import javax.print.attribute.standard.Media;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;


@Path("/dtupay")
public class RestDTUPayAdapter {
    static InMemoryRepository inMemoryRepository = new InMemoryRepository();
    private static DTUPay dtuPay = new DTUPay(inMemoryRepository, inMemoryRepository);


    @POST
    @Consumes("application/json")
    @Path("/testMerchant")
    public void deleteMerchant(MerchantIDDTO merchantIDDTO) throws Exception {
        dtuPay.deleteMerchant(UUID.fromString(merchantIDDTO.getMerchantAccountID()));
        //return dtuPay.getString();
    }


    @POST
    @Consumes("application/json")
    @Path("/testCustomer")
    public void deleteCustomer(CustomerIDDTO customerIDDTO) throws Exception {
        dtuPay.deleteCustomer(UUID.fromString(customerIDDTO.getCustomerAccountID()));
    }

    @Path("/customers")
    public CustomerResource customerResource() {
        return new CustomerResource();
    }

    @Path("/merchants")
    public MerchantsResource merchantsResource() {
        return new MerchantsResource();
    }

    public class CustomerResource {

        @POST
        @Produces("application/json")
        @Consumes("application/json")
        public CustomerIDDTO registerCustomer(DTUPayUserDTO dtuPayUser) throws Exception {
            CustomerIDDTO customerIDDTO = new CustomerIDDTO();
            Customer customer = dtuPay.registerCustomer(dtuPayUser.getCprNumber(), dtuPayUser.getFirstName(), dtuPayUser.getLastName(), dtuPayUser.getAccountID());
            customerIDDTO.setCustomerID(customer.getUserID());
            customerIDDTO.setCustomerAccountID(customer.getAccountID());
            return customerIDDTO;
        }

        @Path("/report")
        public CustomerReportResource customerReportResource() {
            return new CustomerReportResource();
        }

        public class CustomerReportResource {
            @GET
            @Produces(MediaType.APPLICATION_JSON)
            @Consumes("application/json")
            public Response getCustumerReport(@QueryParam("id") String id, @QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate) throws Exception {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
                //convert String to LocalDate
                LocalDate start = LocalDate.parse(startDate, formatter);
                LocalDate end = LocalDate.parse(endDate, formatter);

                List<Report> reports = dtuPay.getReportByCustomerID(id, start, end);
                String json = new Gson().toJson(reports);
                return Response.ok().entity(json).build();
            }
        }


        @Path("/{id}")
        public CustomersIdResource customersIdResource() {
            return new CustomersIdResource();
        }

        public class CustomersIdResource {
            @GET
            @Produces("application/json")
            @Consumes("application/json")
            public DTUPayUserDTO returnCustomerInfo(@PathParam("id") UUID id) {
                DTUPayUserDTO dtuPayUserDTO = new DTUPayUserDTO();
                Customer customer = dtuPay.getCustomerByID(id);
                dtuPayUserDTO.setAccountID(customer.getAccountID());
                dtuPayUserDTO.setCprNumber(customer.getCprNumber());
                dtuPayUserDTO.setFirstName(customer.getFirstName());
                dtuPayUserDTO.setLastName(customer.getLastName());
                return dtuPayUserDTO;
            }
            /*@PUT
            @Consumes(MediaType.APPLICATION_JSON)
            @Produces(MediaType.APPLICATION_JSON)
            public Response setCustomer(@PathParam("id") UUID id, DTUPayUserDTO dtuPayUser) {
                Customer customer = dtuPay.updateCustomer(id, dtuPayUser);
                String json = new Gson().toJson(customer);
                return Response.ok().entity(json).build();
            }*/


        }

        @Path("/{id}/tokens")
        public TokenCustomerResource tokenCustomerResource() {
            return new TokenCustomerResource();
        }

        public class TokenCustomerResource {
            @POST
            @Produces("application/json")
            @Consumes("application/json")

            public TokenListDTO requestNumberOfTokens(@PathParam("id") UUID id, RequestTokenDTO requestTokenDTO) throws Exception {
                TokenListDTO tokenListDTO = new TokenListDTO();
                List<Token> listOfTokens = dtuPay.requestTokens(id, requestTokenDTO.numberOfTokens);
                if(listOfTokens.isEmpty()){
                    throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity("Could not get tokens for user").build());
                }
                tokenListDTO.setTokens(listOfTokens);
                return tokenListDTO;
            }

        }

    }

    public class MerchantsResource {

        @POST
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        public MerchantIDDTO registerMerchant(DTUPayUserDTO dtuPayUser) throws Exception {
            MerchantIDDTO merchantIDDTO = new MerchantIDDTO();
            Merchant merchant = dtuPay.registerMerchant(dtuPayUser.getCprNumber(), dtuPayUser.getFirstName(), dtuPayUser.getLastName(), dtuPayUser.getAccountID());
            merchantIDDTO.setMerchantID(merchant.getUserID());
            merchantIDDTO.setMerchantAccountID(merchant.getAccountID());
            return merchantIDDTO;
        }

        @Path("/{id}")
        public MerchantsIdResource merchantsIdResource() {
            return new MerchantsIdResource();
        }

        public class MerchantsIdResource {
//            @GET
//            @Produces(MediaType.APPLICATION_JSON)
//            public String getMerchant(@PathParam("id") UUID id) {
//                Merchant merchant = dtuPay.getMerchantByID(id);
//                return new Gson().toJson(merchant);
//            }

            @GET
            @Produces("application/json")
            @Consumes("application/json")

            public DTUPayUserDTO returnMerchantInfo(@PathParam("id") UUID id) {
                DTUPayUserDTO dtuPayUserDTO = new DTUPayUserDTO();
                Merchant merchant = dtuPay.getMerchantByID(id);
                dtuPayUserDTO.setAccountID(merchant.getAccountID());
                dtuPayUserDTO.setCprNumber(merchant.getCprNumber());
                dtuPayUserDTO.setFirstName(merchant.getFirstName());
                dtuPayUserDTO.setLastName(merchant.getLastName());
                return dtuPayUserDTO;
            }

            @PUT
            @Consumes(MediaType.APPLICATION_JSON)
            @Produces(MediaType.APPLICATION_JSON)
            public Response setMerchant(@PathParam("id") UUID id, DTUPayUserDTO dtuPayUser) {
                Merchant merchant = dtuPay.updateMerchant(id, dtuPayUser);
                String json = new Gson().toJson(merchant);
                return Response.ok().entity(json).build();
            }

            @DELETE
            @Produces(MediaType.APPLICATION_JSON)
            public Response deleteMerchant(@PathParam("id") UUID id) throws Exception {
                if (dtuPay.doesMerchantExist(id)) {
                    dtuPay.deleteMerchant(id);
                }
                return Response.noContent().build();
            }


        }

        @Path("/transfer")
        public TransferResource transferResource() {
            return new TransferResource();
        }

        public class TransferResource {
            @POST
            @Consumes("application/json")
            public Response transferMoney(PaymentDTO paymentDTO) {
                try {
                    dtuPay.transferMoney(paymentDTO);
                    return Response.ok().build();
                } catch (Exception e) {
                    return Response.noContent().build();
                }

            }
        }

        @Path("/report")
        public ReportResource reportResource() {
            return new ReportResource();
        }

        public class ReportResource {
            @GET
            @Produces(MediaType.APPLICATION_JSON)
            @Consumes("application/json")
            public Response getReport(@QueryParam("id") String id, @QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate) throws Exception {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
                //convert String to LocalDate
                LocalDate start = LocalDate.parse(startDate, formatter);
                LocalDate end = LocalDate.parse(endDate, formatter);

                List<Report> reports = dtuPay.getReportByMerchantID(id, start, end);
                String json = new Gson().toJson(reports);
                return Response.ok().entity(json).build();
            }
        }





/*
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("{id}")
    public Customer ReturnCustomer(@PathParam("id") UUID id){
        //DTUPayUserDTO dtuPayUserDTO = new DTUPayUserDTO();

        //mangler et check om useren eksisterer

        return dtuPay.getCustomerByID(id);
    }
        @GET
        @Consumes("application/json")
        @Produces("application/json")
        public NumberOfUserTokensDTO getUsersNumberOfTokens(UUID customerID){
        @Path("/numberOfTokens")
            NumberOfUserTokensDTO numberOfUserTokensDTO = new NumberOfUserTokensDTO();
        }
            return numberOfUserTokensDTO;
            numberOfUserTokensDTO.setNumberOfUserTokens(dtuPay.getCustomersNumberOfTokens(customerID));

 */


    }

}