package com.dtupay.monolith.Services;

import com.dtupay.monolith.Adapters.Interfaces.IBank;
import com.dtupay.monolith.Adapters.Interfaces.ITokenManager;
import com.dtupay.monolith.DAL.Interfaces.ICustomerRepository;
import com.dtupay.monolith.DAL.Interfaces.IMerchantRepository;
import com.dtupay.monolith.DAL.Interfaces.ITransactionRequestRepository;
import com.dtupay.monolith.DAL.Models.*;
import dtu.ws.fastmoney.Account;
import dtupay.core.DAL.models.Token;

import java.io.InvalidObjectException;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
/*
TODO: Tilføj/refactor TransactionRequestRepository
public class TransactionManager {
    private IBank bank;
    private TokenManager ITokenManager;
    private ITransactionRequestRepository transactionRequestRepository;
    private IMerchantRepository merchantRepository;
    private ICustomerRepository customerRepository;

    public TransactionManager(IBank bank, TokenManager ITokenManager, ITransactionRequestRepository transactionRequestRepository, IMerchantRepository merchantRepository, ICustomerRepository customerRepository) {
        this.bank = bank;
        this.ITokenManager = ITokenManager;
        this.transactionRequestRepository = transactionRequestRepository;
        this.merchantRepository = merchantRepository;
        this.customerRepository = customerRepository;
    }

    public TransactionRequest generateTransaction(Token customerToken, Merchant merchant, BigDecimal amount, Customer customer, String description) {
        TransactionRequest transactionRequest = new TransactionRequest(UUID.randomUUID(), customerToken, merchant, amount, customer, description);
        transactionRequestRepository.addTransaction(transactionRequest);
        return transactionRequest;
    }

    private void executeTransaction(TransactionRequest transactionRequest, DTUPayAccount fromAccount, DTUPayAccount toAccount) throws Exception {
        BigDecimal amount = transactionRequest.getAmount();
        String description = transactionRequest.getDescription();
        bank.transferMoneyFromTo(fromAccount.getId(), toAccount.getId(), amount, description);
        transactionRequest.setTransactionStatus(TransactionRequest.TransactionStatus.SUCCEEDED);
    }

    public void processTransaction(TransactionRequest transactionRequest) throws Exception {
        if(ITokenManager.useToken(transactionRequest.getCustomerToken())) {
            DTUPayAccount merchantAccount = bank.getAccountByCPRNumber(transactionRequest.getMerchant().getCprNumber());
            DTUPayAccount customerAccount = bank.getAccountByCPRNumber(transactionRequest.getCustomer().getCprNumber());
            executeTransaction(transactionRequest, customerAccount, merchantAccount);
            updateEntitiesAfterTransfer(transactionRequest);
        }
    }

    public void processRefund(TransactionRequest transactionRequest) throws Exception {
        if(ITokenManager.useToken(transactionRequest.getCustomerToken())) {
            DTUPayAccount merchantAccount = bank.getAccountByCPRNumber(transactionRequest.getMerchant().getCprNumber());
            DTUPayAccount customerAccount = bank.getAccountByCPRNumber(transactionRequest.getCustomer().getCprNumber());
            executeTransaction(transactionRequest, merchantAccount, customerAccount);
            updateEntitiesAfterTransfer(transactionRequest);
        }
    }

    private void updateEntitiesAfterTransfer(TransactionRequest transactionRequest) {
        transactionRequestRepository.updateTransactionRequest(transactionRequest);
        Merchant merchant = transactionRequest.getMerchant();
        List<TransactionRequest> transactionRequests = merchant.getTransactionRequests();
        transactionRequests.add(transactionRequest);
        merchant.setTransactionRequests(transactionRequests);
        merchantRepository.updateMerchant(merchant);
        Customer customer = transactionRequest.getCustomer();
        List<TransactionRequest> transactionRequestsCustomer = customer.getTransactionRequests();
        transactionRequestsCustomer.add(transactionRequest);
        customer.setTransactionRequests(transactionRequestsCustomer);
        customerRepository.update(customer);
    }

    public boolean isTransactionSucceeded(String transactionRequestID) {
        return TransactionRequest.TransactionStatus.SUCCEEDED == transactionRequestRepository.getTransactionRequest(UUID.fromString(transactionRequestID)).getTransactionStatus();
    }
}
*/