package com.dtupay.monolith.Services;

import com.dtupay.monolith.Adapters.Interfaces.ITokenManager;
import com.dtupay.monolith.DAL.Interfaces.ICustomerRepository;
import com.dtupay.monolith.DAL.Interfaces.ITokenRepository;
import dtupay.core.DAL.models.Token;
import dtupay.core.Enums.TokenStatus;

import java.io.InvalidObjectException;
import java.util.*;
/*
public class TokenManager implements ITokenManager {
    private ITokenRepository tokenRepository;
    private ICustomerRepository customerRepository;

    public TokenManager(ITokenRepository tokenRepository, ICustomerRepository customerRepository) {
        this.tokenRepository = tokenRepository;
        this.customerRepository = customerRepository;
    }

    @Override
    public int getCustomersNumberOfTokens(UUID customerID) {
        return customerRepository.getCustomerByID(customerID).numberOfTokens();
    }

    @Override
    public List<Token> requestTokens(UUID customerID, int requestedNumberOfTokens) {
        int numberOfCustomerTokens = customerRepository.getCustomerByID(customerID).numberOfTokens();
        if (numberOfCustomerTokens == 0 || requestedNumberOfTokens == 1) {
            return generateTokens(requestedNumberOfTokens);
        } else {
            return Collections.emptyList();
        }
    }

    //TODO: Change name / method.
    public boolean validateToken(Token token) {
        if(!doesTokenExist(token))
            return false;
        if (isTokenUsed(token.getTokenID())){
            token.setStatus(TokenStatus.REJECTED);
            tokenRepository.updateToken(token);
            return false;
        }
        return true;
    }

    private boolean doesTokenExist(Token token) {
        return tokenRepository.doesTokenExist(token);
    }

    public boolean useToken(Token token) throws InvalidObjectException {
        if(!validateToken(token)) {
            throw new InvalidObjectException("Invalid Token");
        }
        token.setStatus(TokenStatus.ACCEPTED); // Temporary
        tokenRepository.updateToken(token);
        return true;
    }

    public void removeToken(String tokenId) {
        tokenRepository.removeToken(tokenId);
    }

    //TODO: Change to return type Optional(Token Repo) & change input to tokenID (String) instead of Token.
    public boolean wasAccepted(String tokenID) {
        Token dbToken = tokenRepository.getTokenByID(tokenID);
        if(dbToken == null){
            return false;
        }
        return dbToken.getStatus() == TokenStatus.ACCEPTED;
    }

    private List<Token> generateTokens(int numberOfTokens){
        ArrayList<Token> generatedTokens = new ArrayList<>();
        String uuid;
        for(int i = 0; i < numberOfTokens; i++){
            uuid = UUID.randomUUID().toString();
            Token newToken = new Token(uuid, UUID.randomUUID());
            newToken.setStatus(TokenStatus.UNUSED);
            generatedTokens.add(newToken);
            tokenRepository.addToken(newToken);
        }//TODO: Change to return type Optional(Token Repo).
        return generatedTokens;
    }
//TODO: Change to tokenID.

    public boolean isTokenUsed(String tokenID) {
        Token dbToken = tokenRepository.getTokenByID(tokenID);
        return dbToken.getStatus() != TokenStatus.UNUSED;
    }
}
*/
