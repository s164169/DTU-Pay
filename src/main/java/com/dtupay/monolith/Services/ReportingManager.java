package com.dtupay.monolith.Services;

import com.dtupay.monolith.Adapters.Interfaces.IReportingManager;
import com.dtupay.monolith.DAL.Interfaces.ICustomerRepository;
import com.dtupay.monolith.DAL.Interfaces.IMerchantRepository;
import com.dtupay.monolith.Models.Report;
import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.Merchant;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
/*
public class ReportingManager implements IReportingManager {

    private IMerchantRepository merchantRepository;
    private ICustomerRepository customerRepository;

    public ReportingManager(IMerchantRepository merchantRepository,
                            ICustomerRepository customerRepository) {

        this.merchantRepository = merchantRepository;
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Report> getReportByMerchantID(UUID merchantID) {
        Merchant merchant = merchantRepository.getMerchantByID(merchantID);
        List<Report> reports = merchant
                .getTransactionRequests()
                .stream()
                .map(x -> new Report(x.getCustomerToken(), x.getMerchant().getUserID(), x.getAmount(), x.getDescription()))
                .collect(Collectors.toList());
        return reports;
    }

    @Override
    public List<Report> getReportByCustomerID(UUID customerID) {
        Customer customer = customerRepository.getCustomerByID(customerID);
        List<Report> reports = customer
                .getTransactionRequests()
                .stream()
                .map(x -> new Report(x.getCustomerToken(), x.getMerchant().getUserID(), x.getCustomer().getUserID(), x.getAmount(), x.getDescription()))
                .collect(Collectors.toList());
        return reports;
    }

    @Override
    public void createMerchant(Merchant merchant) throws Exception {

    }

}
*/
