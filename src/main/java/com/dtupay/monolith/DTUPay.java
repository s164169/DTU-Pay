package com.dtupay.monolith;

import com.dtupay.monolith.DAL.Interfaces.ICustomerRepository;
import com.dtupay.monolith.DAL.Interfaces.IMerchantRepository;
import com.dtupay.monolith.DAL.Models.*;
import com.dtupay.monolith.Models.DTUPayUserDTO;
import com.dtupay.monolith.Models.Report;
import com.dtupay.monolith.Receivers.DtuPayReceiver;
import com.dtupay.monolith.Receivers.Factories.DtuPayServiceFactory;
import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.DTUPayUser;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.DAL.models.Token;
import dtupay.core.models.DTOs.PaymentDTO;
import dtupay.core.models.DTOs.TransferMoneyDTO;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

/*
      ___           ___           ___                    ___           ___           ___
     /\  \         /\  \         /\__\                  /\  \         /\  \         |\__\
    /::\  \        \:\  \       /:/  /                 /::\  \       /::\  \        |:|  |
   /:/\:\  \        \:\  \     /:/  /                 /:/\:\  \     /:/\:\  \       |:|  |
  /:/  \:\__\       /::\  \   /:/  /  ___            /::\~\:\  \   /::\~\:\  \      |:|__|___
 /:/__/ \:|__|     /:/\:\__\ /:/__/  /\__\          /:/\:\ \:\__\ /:/\:\ \:\__\     /::::\___\
 \:\  \ /:/  /    /:/  \/__/ \:\  \ /:/  /          \/__\:\/:/  / \/__\:\/:/  /    /:/~~/
  \:\  /:/  /    /:/  /       \:\  /:/  /                \::/  /       \::/  /    /:/  /
   \:\/:/  /     \/__/         \:\/:/  /                  \/__/        /:/  /     \/__/
    \::/__/                     \::/  /                               /:/  /
     ~~                          \/__/                                \/__/
*/

public class DTUPay {
    ICustomerRepository customerRepository;
    IMerchantRepository merchantRepository;

    public DTUPay(ICustomerRepository customerRepository, IMerchantRepository merchantRepository) {
        this.customerRepository = customerRepository;
        this.merchantRepository = merchantRepository;
    }

    //TODO: Fjern dette når færdigt
    public String getString() {
        return "TestDTUPay";
    }

    //TODO: Ændrer det her shit til at broadcaste create customer
    public Customer registerCustomer(String cprNr, String firstName, String lastName, String accountID) throws Exception {
        DtuPayReceiver dtuPayReceiver = new DtuPayServiceFactory().getService();
        Customer customer = new Customer(cprNr, firstName, lastName, UUID.randomUUID(), accountID);
        customerRepository.addCustomer(customer);
        dtuPayReceiver.createCustomer(customer);
        return customer;
    }

    public Merchant registerMerchant(String cprNr, String firstName, String lastName, String accountID) throws Exception {
        DtuPayReceiver dtuPayReceiver = new DtuPayServiceFactory().getService();
        Merchant merchant = new Merchant(cprNr, firstName, lastName, UUID.randomUUID(), accountID);
        merchantRepository.addMerchant(merchant);
        dtuPayReceiver.createMerchant(merchant);
        return merchant;
    }

    public Customer updateCustomer(UUID userID, DTUPayUser input) throws Exception {
        DtuPayReceiver dtuPayReceiver = new DtuPayServiceFactory().getService();
        Customer customer = getCustomerByID(userID);
        customer.setFirstName(input.getFirstName());
        customer.setLastName(input.getLastName());
        customer.setCprNumber(input.getCprNumber());
        customer.setAccountID(input.getAccountID());
        customerRepository.update(customer);
        dtuPayReceiver.updateCustomer(customer);
        return customer;
    }

    public Merchant updateMerchant(UUID userID, DTUPayUser input) throws Exception{
        DtuPayReceiver dtuPayReceiver = new DtuPayServiceFactory().getService();
        Merchant merchant = getMerchantByID(userID);
        merchant.setFirstName(input.getFirstName());
        merchant.setLastName(input.getLastName());
        merchant.setCprNumber(input.getCprNumber());
        merchant.setAccountID(input.getAccountID());
        merchantRepository.updateMerchant(merchant);
        dtuPayReceiver.updateMerchant(merchant);
        return merchant;
    }


    public List<Token> requestTokens(UUID customerID, int numberOfTokens) throws Exception {
        DtuPayReceiver dtuPayReceiver = new DtuPayServiceFactory().getService();
        return dtuPayReceiver.requestTokens(customerID, numberOfTokens);
    }


    public List<Report> getReportByMerchantID(String merchantID, LocalDate start, LocalDate end) throws Exception {
        DtuPayReceiver dtuPayReceiver = new DtuPayServiceFactory().getService();
        return dtuPayReceiver.getReportByMerchantID(UUID.fromString(merchantID), start, end);
    }

    public List<Report> getReportByCustomerID(String customerID, LocalDate start, LocalDate end) throws Exception {
        DtuPayReceiver dtuPayReceiver = new DtuPayServiceFactory().getService();
        return dtuPayReceiver.getReportByCustomerID(UUID.fromString(customerID), start, end);
    }

    //TODO: Ændrer validateToken til at tage id i stedet for token
    public boolean isTokenValid(Token token) throws Exception {
        DtuPayReceiver dtuPayReceiver = new DtuPayServiceFactory().getService();
        return dtuPayReceiver.validateToken(token);
    }


    public int getCustomersNumberOfTokens(UUID customerID) throws Exception {
        DtuPayReceiver dtuPayReceiver = new DtuPayServiceFactory().getService();
        return dtuPayReceiver.getCustomersNumberOfTokens(customerID);
    }

    //TODO: HVORDAN FUCK GØR VI DET HER
    public Merchant getMerchantByID(UUID merchantID){
        return merchantRepository.getMerchantByID(merchantID);
    }

    public Customer getCustomerByID(UUID customerID){
        return customerRepository.getCustomerByID(customerID);
    }

    //TODO: TILFØJ DET HER SAMMEN MED UPDATE CUSTOM
    public Merchant updateMerchant(UUID merchantID, DTUPayUserDTO dtuPayUser) {
        Merchant merchant = merchantRepository.getMerchantByID(merchantID);
        merchant.setCprNumber(dtuPayUser.getCprNumber());
        merchant.setFirstName(dtuPayUser.getFirstName());
        merchant.setLastName(dtuPayUser.getFirstName());
        merchant.setAccountID(dtuPayUser.getFirstName());
        merchantRepository.updateMerchant(merchant);
        return merchant;
    }

    public Customer updateCustom() throws Exception {
        throw new Exception();
    }

    public void deleteMerchant(UUID merchantID) throws Exception {
        DtuPayReceiver dtuPayReceiver = new DtuPayServiceFactory().getService();
        dtuPayReceiver.deleteMerchant(merchantID);
        merchantRepository.deleteMerchant(merchantID);
    }

    public void deleteCustomer(UUID customerID) throws Exception {
        DtuPayReceiver dtuPayReceiver = new DtuPayServiceFactory().getService();
        dtuPayReceiver.deleteCustomer(customerID);
        customerRepository.removeCustomer(customerID);
    }

    public boolean doesMerchantExist(UUID merchantID){
        return merchantRepository.doesMerchantExist(merchantID);
    }

    public boolean dcustomerHasTokensResponseoesCustomerExist(UUID customerID) {
        return customerRepository.doesCustomerExist(customerID);
    }

    public void transferMoney(PaymentDTO paymentDTO) throws Exception {
        DtuPayReceiver dtuPayReceiver = new DtuPayServiceFactory().getService();
        dtuPayReceiver.transferMoney(paymentDTO);
    }

    public void refundPayment(PaymentDTO paymentDTO) throws Exception {
        DtuPayReceiver dtuPayReceiver = new DtuPayServiceFactory().getService();
        dtuPayReceiver.refundPayment(paymentDTO);
    }

}
