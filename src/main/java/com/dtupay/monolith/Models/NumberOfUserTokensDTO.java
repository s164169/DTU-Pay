package com.dtupay.monolith.Models;

public class NumberOfUserTokensDTO {
    public int getNumberOfUserTokens() {
        return numberOfUserTokens;
    }

    public void setNumberOfUserTokens(int numberOfUserTokens) {
        this.numberOfUserTokens = numberOfUserTokens;
    }

    public int numberOfUserTokens;
}
