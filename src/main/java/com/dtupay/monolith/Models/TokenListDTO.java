package com.dtupay.monolith.Models;

import dtupay.core.DAL.models.Token;

import java.util.List;

public class TokenListDTO {
    public List<Token> tokens;

    public List<Token> getTokens() {
        return tokens;
    }

    public void setTokens(List<Token> tokens) {
        this.tokens = tokens;
    }
}
