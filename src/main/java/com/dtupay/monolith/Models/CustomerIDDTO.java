package com.dtupay.monolith.Models;

import java.util.UUID;

public class CustomerIDDTO {

    public UUID customerID;
    public String customerAccountID;

    public UUID getCustomerID() {
        return customerID;
    }

    public void setCustomerID(UUID customerID) {
        this.customerID = customerID;
    }

    public String getCustomerAccountID() {
        return customerAccountID;
    }

    public void setCustomerAccountID(String customerAccountID) {
        this.customerAccountID = customerAccountID;
    }
}

