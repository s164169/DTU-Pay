package com.dtupay.monolith.Models;

import java.time.LocalDate;
import java.util.UUID;

public class GetReportDTO {
    UUID ID;
    LocalDate startDate;
    LocalDate endDate;
}
