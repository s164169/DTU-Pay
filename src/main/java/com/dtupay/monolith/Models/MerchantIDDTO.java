package com.dtupay.monolith.Models;

import java.util.UUID;

public class MerchantIDDTO {
    public UUID merchantID;
    private String merchantAccountID;


    public UUID getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(UUID merchantID) {
        this.merchantID = merchantID;
    }

    public String getMerchantAccountID() {
        return merchantAccountID;
    }

    public void setMerchantAccountID(String merchantAccountID) {
        this.merchantAccountID = merchantAccountID;
    }
}
