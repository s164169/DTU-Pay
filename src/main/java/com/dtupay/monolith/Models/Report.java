package com.dtupay.monolith.Models;

import dtupay.core.DAL.models.Token;

import java.math.BigDecimal;
import java.util.UUID;

public class Report {
    private Token customerToken;
    private UUID merchantID;
    private UUID customerID;
    private BigDecimal amount;
    private String description;

    public Report(Token customerToken, UUID merchantID, BigDecimal amount, String description) {
        this.customerToken = customerToken;
        this.merchantID = merchantID;
        this.amount = amount;
        this.description = description;
    }

    public Report(Token customerToken, UUID merchantID, UUID customerID, BigDecimal amount, String description) {
        this.customerToken = customerToken;
        this.merchantID = merchantID;
        this.customerID = customerID;
        this.amount = amount;
        this.description = description;
    }

    public UUID getCustomer() {
        return customerID;
    }

    public void setCustomer(UUID customerID) {
        this.customerID = customerID;
    }

    public Token getCustomerToken() {
        return customerToken;
    }

    public void setCustomerToken(Token customerToken) {
        this.customerToken = customerToken;
    }

    public UUID getMerchant() {
        return merchantID;
    }

    public void setMerchant(UUID merchantID) {
        this.merchantID = merchantID;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
