package com.dtupay.monolith.DAL.Interfaces;

import dtupay.core.DAL.models.TransactionRequest;

import java.util.UUID;

public interface ITransactionRequestRepository {
    void addTransaction(TransactionRequest transactionRequest);

    TransactionRequest getTransactionRequest(UUID transactionRequestID);

    void updateTransactionRequest(TransactionRequest transactionRequest);

}
