package com.dtupay.monolith.DAL.Interfaces;

import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.DTUPayUser;

import java.util.ArrayList;
import java.util.UUID;

public interface ICustomerRepository {

    Customer getCustomerByID(UUID customerID);

    ArrayList<Customer> getAllCustomers();

    void addCustomer(Customer customer);

    void removeCustomer(UUID customerID);

    void update(Customer customer);

    boolean doesCustomerExist(UUID customerID);
}
