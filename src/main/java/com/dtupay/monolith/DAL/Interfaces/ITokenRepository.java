package com.dtupay.monolith.DAL.Interfaces;

import dtupay.core.DAL.models.Token;

public interface ITokenRepository {

    Token getTokenByID(String tokenID);

    void addToken(Token newToken);

    void removeToken(String tokenID);

    boolean doesTokenExist(Token token);

    void updateToken(Token token);
}
