package com.dtupay.monolith.DAL.Interfaces;

import dtupay.core.DAL.models.Merchant;

import java.util.ArrayList;
import java.util.UUID;

public interface IMerchantRepository {

    Merchant getMerchantByID(UUID merchantID);

    ArrayList<Merchant> getAllMerchants();

    void addMerchant(Merchant merchant);

    void deleteMerchant(UUID merchantID);

    void updateMerchant(Merchant merchant);

    boolean doesMerchantExist(UUID merchantID);
}
