package com.dtupay.monolith.DAL;

import com.dtupay.monolith.DAL.Interfaces.ICustomerRepository;
import com.dtupay.monolith.DAL.Interfaces.IMerchantRepository;
import com.dtupay.monolith.DAL.Interfaces.ITokenRepository;
import com.dtupay.monolith.DAL.Interfaces.ITransactionRequestRepository;
import dtupay.core.DAL.models.*;

import java.math.BigDecimal;
import java.util.*;

public class InMemoryRepository implements ICustomerRepository, IMerchantRepository, ITokenRepository, ITransactionRequestRepository {
    private static InMemoryRepository instance = new InMemoryRepository();

    public static InMemoryRepository getInstance() {
        return instance;
    }

    private Map<String, Token> tokens = new HashMap<>();
    private Map<UUID, Customer> customers = new HashMap<>();
    private Map<UUID, Merchant> merchants = new HashMap<>();
    private Map<UUID, TransactionRequest> transactionRequests = new HashMap<>();

    @Override
    public Token getTokenByID(String tokenID) {
        return tokens.get(tokenID);
    }

    @Override
    public void addToken(Token newToken) {
        tokens.put(newToken.getTokenID(), newToken);

    }

    @Override
    public void removeToken(String tokenID) {
        tokens.remove(tokenID);
    }

    @Override
    public boolean doesTokenExist(Token token) {
        return tokens.containsValue(token);
    }

    @Override
    public void updateToken(Token token) {
        tokens.replace(token.getTokenID(), token);
    }

    @Override
    public Customer getCustomerByID(UUID customerID) {
        return customers.get(customerID);
    }

    @Override
    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> allCustomers = new ArrayList<>();
        for (UUID key : customers.keySet()) {
            allCustomers.add(customers.get(key));
        }
        return allCustomers;
    }

    @Override
    public void addCustomer(Customer customer) {
        customers.put(customer.getUserID(), customer);
    }

    @Override
    public void removeCustomer(UUID customerID) {
        merchants.remove(customerID);
    }

    @Override
    public void update(Customer customer) {
        customers.replace(customer.getUserID(), (Customer) customer);
    }

    @Override
    public boolean doesCustomerExist(UUID customerID) {
        if (customers.get(customerID)!=null){
            return true;
        }
        return false;
    }

    @Override
    public Merchant getMerchantByID(UUID merchantID) {
        return merchants.get(merchantID);
    }

    @Override
    public ArrayList<Merchant> getAllMerchants() {
        ArrayList<Merchant> allMerchants = new ArrayList<>();
        for (UUID key : merchants.keySet()) {
            allMerchants.add(merchants.get(key));
        }
        return allMerchants;
    }

    @Override
    public boolean doesMerchantExist(UUID merchantID){
        if (merchants.get(merchantID)!=null){
            return true;
        }
        return false;
    }

    @Override
    public void addMerchant(Merchant merchant) {
        merchants.put(merchant.getUserID(), merchant);
    }

    @Override
    public void deleteMerchant(UUID merchantID) {
        merchants.remove(merchantID);
    }

    @Override
    public void updateMerchant(Merchant merchant) {
        merchants.replace(merchant.getUserID(), merchant);
    }

    @Override
    public void addTransaction(TransactionRequest transactionRequest) {
        transactionRequests.put(transactionRequest.getTransactionRequestID(), transactionRequest);
    }

    @Override
    public TransactionRequest getTransactionRequest(UUID transactionRequestID) {
        return transactionRequests.get(transactionRequestID);
    }

    @Override
    public void updateTransactionRequest(TransactionRequest transactionRequest) {
        transactionRequests.replace(transactionRequest.getTransactionRequestID(), transactionRequest);
    }

}
