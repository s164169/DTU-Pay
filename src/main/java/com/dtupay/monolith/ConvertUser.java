package com.dtupay.monolith;

import dtu.ws.fastmoney.User;
import dtupay.core.DAL.models.DTUPayUser;

public class ConvertUser {

    public static User covertUser(DTUPayUser dtuPayUser) {
        User user = new User();
        user.setCprNumber(dtuPayUser.getCprNumber());
        user.setFirstName(dtuPayUser.getFirstName());
        user.setLastName(dtuPayUser.getLastName());
        return user;
    }
}
