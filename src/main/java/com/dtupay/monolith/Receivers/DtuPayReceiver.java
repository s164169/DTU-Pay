package com.dtupay.monolith.Receivers;

import com.dtupay.monolith.Adapters.Interfaces.IReportingManager;
import com.dtupay.monolith.Adapters.Interfaces.ITokenManager;
import com.dtupay.monolith.Adapters.Interfaces.ITransactionManager;
import com.dtupay.monolith.Models.Report;
import com.google.gson.Gson;
import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.DAL.models.Token;
import dtupay.core.models.DTOs.PaymentDTO;
import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;

import javax.naming.OperationNotSupportedException;
import javax.ws.rs.NotAllowedException;
import java.io.InvalidObjectException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class DtuPayReceiver implements EventReceiver, ITokenManager, ITransactionManager, IReportingManager {


    private EventSender sender;
    private CompletableFuture<Event> future;

    private static final String TRANSFER_MONEY_FROM_TO = "transferMoneyFromTo";
    private static final String TRANSFER_MONEY_FROM_TO_SUCCEEDED = "transferMoneyFromToSucceeded";
    private static final String REFUND_PAYMENT = "RefundPayment";
    private static final String REFUND_PAYMENT_SUCCEEDED = "RefundPaymentSucceeded";
    private static final String COSTUMER_AVAILABLE_TOKENS = "CostumerAvailableTokens";
    private static final String COSTUMER_AVAILABLE_TOKENS_SUCCEEDED = "CostumerAvailableTokensSucceeded";
    private static final String REQUEST_TOKENS = "RequestTokens";
    private static final String REQUEST_TOKENS_FAILED = "RequestTokensFailed";
    private static final String REQUEST_TOKENS_SUCCEEDED = "RequestTokensSucceeded";
    private static final String VALIDATE_TOKEN = "ValidateToken";
    private static final String VALIDATE_TOKEN_SUCCEEDED = "ValidateTokenSucceeded";
    private static final String DELETE_CUSTOMER = "DeleteCustomer";
    private static final String CREATE_CUSTOMER = "CreateCustomer";
    private static final String CREATE_MERCHANT = "CreateMerchant";
    private static final String DELETE_MERCHANT = "DeleteMerchant";
    private static final String UPDATE_CUSTOMER = "UpdateCustomer";
    private static final String UPDATE_MERCHANT = "UpdateMerchant";
    private static final String GET_REPORT_BY_MERCHANT_ID = "GetReportByMerchantID";
    private static final String GET_REPORT_BY_MERCHANT_ID_SUCCEEDED = "GetReportByMerchantIDSucceeded";
    private static final String GET_REPORT_BY_CUSTOMER_ID = "GetReportByCustomerID";
    private static final String GET_REPORT_BY_CUSTOMER_ID_SUCCEEDED = "GetReportByCustomerIDSucceeded";

    public DtuPayReceiver(EventSender sender) {
        this.sender = sender;
    }

    @Override
    public void receiveEvent(Event event) throws Exception {
        switch (event.getEventType()) {
            case TRANSFER_MONEY_FROM_TO_SUCCEEDED:
            case REFUND_PAYMENT_SUCCEEDED:
            case COSTUMER_AVAILABLE_TOKENS_SUCCEEDED:
            case REQUEST_TOKENS_SUCCEEDED:
            case VALIDATE_TOKEN_SUCCEEDED:
            case GET_REPORT_BY_MERCHANT_ID_SUCCEEDED:
            case GET_REPORT_BY_CUSTOMER_ID_SUCCEEDED:
            case REQUEST_TOKENS_FAILED:
                System.out.println("Event handled " + event);
                future.complete(event);
                break;
            default:
                System.out.println("Ignoring event: " + event);
                break;
        }

    }

    private Event sendEvent(String eventDescription, Object[] args) throws Exception {
        Event event = new Event(eventDescription, args);
        future = new CompletableFuture<>();
        sender.sendEvent(event);
        return future.join();
    }

    private void sendEventWithOutWaiting(String eventDescription, Object[] args) throws Exception {
        Event event = new Event(eventDescription, args);
        sender.sendEvent(event);
    }

    public void transferMoney(PaymentDTO paymentDTO) throws Exception {
        Object[] args = {paymentDTO};
        Event transferMoneyFromToEvent = sendEvent(TRANSFER_MONEY_FROM_TO, args);
        if (!TRANSFER_MONEY_FROM_TO_SUCCEEDED.equals(transferMoneyFromToEvent.getEventType())) {
            throw new InvalidObjectException("Failed to transfer money");
        }
    }


    @Override
    public void refundPayment(PaymentDTO paymentDTO) throws Exception {
        Object[] args = {paymentDTO};
        Event transferMoneyFromToEvent = sendEvent(REFUND_PAYMENT, args);
        if (!REFUND_PAYMENT_SUCCEEDED.equals(transferMoneyFromToEvent.getEventType())) {
            throw new InvalidObjectException("Failed to transfer money");
        }
    }

    @Override
    public int getCustomersNumberOfTokens(UUID customerID) throws Exception {
        Object[] args = {customerID};
        Event getAvailableTokensEvent = sendEvent(COSTUMER_AVAILABLE_TOKENS, args);
        if (!COSTUMER_AVAILABLE_TOKENS_SUCCEEDED.equals(getAvailableTokensEvent.getEventType())) {
            throw new Exception("Failed to get number of tokens for costumer");
        }

        return (int) getAvailableTokensEvent.getArguments()[0];
    }

    @Override
    public List<Token> requestTokens(UUID customerID, int requestedNumberOfTokens) throws Exception {
        Object[] args = {customerID, requestedNumberOfTokens};
        Event requestTokensEvent = sendEvent(REQUEST_TOKENS, args);
        if (!REQUEST_TOKENS_SUCCEEDED.equals(requestTokensEvent.getEventType())) {
            return new ArrayList<>();
        }
        Gson gson = new Gson();
        //TODO: TEST DET HER
        return gson.fromJson(gson.toJson(requestTokensEvent.getArguments()[0]), List.class);
    }

    @Override
    public boolean validateToken(Token token) throws Exception {
        Object[] args = {token};
        Event validateTokenEvent = sendEvent(VALIDATE_TOKEN, args);
        if (!VALIDATE_TOKEN_SUCCEEDED.equals(validateTokenEvent.getEventType())) {
            throw new Exception("Failed when trying to validate token");
        }

        return (boolean) validateTokenEvent.getArguments()[0];
    }

    public void deleteCustomer(UUID customerID) throws Exception {
        Object[] args = {customerID};
        sendEventWithOutWaiting(DELETE_CUSTOMER, args);
    }

    public void deleteMerchant(UUID merchantID) throws Exception {
        Object[] args = {merchantID};
        sendEventWithOutWaiting(DELETE_MERCHANT, args);
    }

    @Override
    public List<Report> getReportByMerchantID(UUID merchantID, LocalDate start, LocalDate end) throws Exception {
        Object[] args = {merchantID, start, end};
        Event getReportEvent = sendEvent(GET_REPORT_BY_MERCHANT_ID, args);
        if (!GET_REPORT_BY_MERCHANT_ID_SUCCEEDED.equals(getReportEvent.getEventType())) {
            throw new Exception("Failed to request tokens");
        }
        Gson gson = new Gson();
        //TODO: TEST DET HER
        return gson.fromJson(gson.toJson(getReportEvent.getArguments()[0]), List.class);
    }

    @Override
    public List<Report> getReportByCustomerID(UUID customerID, LocalDate start, LocalDate end) throws Exception {
        Object[] args = {customerID, start, end};
        Event getReportEvent = sendEvent(GET_REPORT_BY_CUSTOMER_ID, args);
        if (!GET_REPORT_BY_CUSTOMER_ID_SUCCEEDED.equals(getReportEvent.getEventType())) {
            throw new Exception("Failed to request tokens");
        }
        Gson gson = new Gson();
        //TODO: TEST DET HER
        return gson.fromJson(gson.toJson(getReportEvent.getArguments()[0]), List.class);
    }

    @Override
    public void createMerchant(Merchant merchant) throws Exception {
        Object[] args = {merchant};
        sendEventWithOutWaiting(CREATE_MERCHANT,args);
    }

    public void createCustomer(Customer customer) throws Exception {
        Object[] args = {customer};
        sendEventWithOutWaiting(CREATE_CUSTOMER,args);
    }

    public void updateCustomer(Customer customer) throws Exception {
        Object[] args = {customer};
        sendEventWithOutWaiting(UPDATE_CUSTOMER,args);
    }

    public void updateMerchant(Merchant merchant) throws Exception {
        Object[] args = {merchant};
        sendEventWithOutWaiting(UPDATE_MERCHANT,args);
    }
}
