package com.dtupay.monolith.Receivers.Factories;

import com.dtupay.monolith.Receivers.DtuPayReceiver;
import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;

public class DtuPayServiceFactory {

    public static DtuPayReceiver service = null;

    public DtuPayReceiver getService(){
        if(service!=null){
            return service;
        }
        EventSender eventSender = new RabbitMqSender();
        service = new DtuPayReceiver(eventSender);
        RabbitMqListener rabbitMqListener = new RabbitMqListener(service);
        try{
            rabbitMqListener.listen();
        } catch (Exception e) {
            throw new Error(e);
        }
        return service;
    }
}
