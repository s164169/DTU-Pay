#
Feature: Tokens

  Scenario: Obtain Tokens
    Given I have 0 tokens
    When I request 1 tokens
    Then I now have 1 tokens

  Scenario: Obtain Tokens
    Given I have 0 tokens
    When I request 2 tokens
    Then I now have 2 tokens

  Scenario: Obtain Tokens
    Given I have 0 tokens
    When I request 3 tokens
    Then I now have 3 tokens

  Scenario: Obtain Tokens
    Given I have 0 tokens
    When I request 4 tokens
    Then I now have 4 tokens

  Scenario: Obtain Tokens
    Given I have 0 tokens
    When I request 5 tokens
    Then I now have 5 tokens

  Scenario: Obtain Tokens
    Given I have 0 tokens
    When I request 6 tokens
    Then I now have 0 tokens

  Scenario: Obtain Tokens
    Given I have 1 tokens
    When I request 1 tokens
    Then I now have 2 tokens

  Scenario: Obtain Tokens
    Given I have 1 tokens
    When I request 2 tokens
    Then I now have 3 tokens

  Scenario: Obtain Tokens
    Given I have 1 tokens
    When I request 3 tokens
    Then I now have 4 tokens

  Scenario: Obtain Tokens
    Given I have 1 tokens
    When I request 4 tokens
    Then I now have 5 tokens

  Scenario: Obtain Tokens
    Given I have 1 tokens
    When I request 5 tokens
    Then I now have 6 tokens

  Scenario: Obtain Tokens
    Given I have 1 tokens
    When I request 6 tokens
    Then I now have 1 tokens

  Scenario: Obtain Tokens
    Given I have 2 tokens
    When I request 5 tokens
    Then I now have 2 tokens

  Scenario: Use token
    Given I have 1 unused token
    When I use a token
    Then The token is accepted

  Scenario: Token was already used
    Given I have 1 used token
    When I use a token
    Then the token is rejected

  Scenario: Fake Token
    Given I have 1 fake token
    When I use a token
    Then the token is rejected