# new feature
# Tags: optional

Feature: Reporting

  Scenario: A merchant wants a report of all transactions he has made
    Given A merchant has a bank account - Reporting
    And A customer has a bank account - Reporting
    And Another customer has a bank account
    And The first customer has made 1 valid transaction to the merchant
    And The second customer has made 1 valid transaction to the merchant
    When The merchant requests a report of all transactions
    Then A list of 2 transactions is received

  Scenario: A customer want a report of all his transactions
    Given A merchant has a bank account - Reporting
    And A customer has a bank account - Reporting
    And The first customer has made 2 valid transaction to the merchant
    When The customer wants a report of all the customers transactions
    Then A list of 2 transactions is received