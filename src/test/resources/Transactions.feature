Feature: Transactions
  Background:
    Given A merchant has a bank account
    And A customer has a bank account

  Scenario: Request payment with unused token
    Given A merchant scans 1 unused customer token
    When The merchant requests payment of 50 DKK with the customer's token
    Then The payment succeeds


  Scenario: Request payment with used token
    Given A merchant scans 1 used customer token
    When The merchant requests payment of 50 DKK with the customer's token
    Then The payment does not succeed


  Scenario: Request payment with fake token
    Given A merchant scans 1 fake customer token
    When The merchant requests payment of 50 DKK with the customer's token
    Then The payment does not succeed


