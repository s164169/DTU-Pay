package com.dtupay.monolith;

/*
public class ReportingSteps {

    private final ITokenManager ITokenManager;
    private Customer customer;
    private Customer secondCustomer;
    private Merchant merchant;
    private TransactionManager transactionManager;
    private ReportingManager reportingManager;
    private TransactionRequest transactionRequest;
    private DTUPay dtuPay;
    private List<String> accounts = new ArrayList<>();
    private List<Report> report;

    public ReportingSteps(Helper helper) {
        //this.transactionManager = helper.getTransactionManager();  --- til mono
        //this.dtuPay = helper.getDtuPay();                          --- til mono
        this.transactionManager = helper.getTransactionManagerSoap();
        this.reportingManager = helper.getReportingManager();
        this.dtuPay = helper.getDtuPaySoap();
        this.ITokenManager = helper.getTokenManager();

    }

    @After
    public void cleanupUsedAccounts() throws Exception {
        for (String account : accounts) {
            dtuPay.retireAccount(account);
        }
    }


    @Given("^A merchant has a bank account - Reporting$")
    public void aMerchantHasABankAccountReporting() throws Exception {
        this.merchant = dtuPay.registerMerchant("375","xx1","zz1", "123456789");
        accounts.add(merchant.getAccountID());
        //dtuPay
    }

    @And("^A customer has a bank account - Reporting$")
    public void aCustomerHasABankAccountReporting() throws Exception {
        this.customer = dtuPay.registerCustomer("426","yy2","zy2", "123456789");
        accounts.add(customer.getAccountID());
    }
    @Given("^Another customer has a bank account$")
    public void anotherCustomerHasABankAccount() throws Exception {
        this.secondCustomer = dtuPay.registerCustomer("1234","yy3","zy3", "123456789");
        accounts.add(secondCustomer.getAccountID());
    }

    @And("^The first customer has made (\\d+) valid transaction to the merchant$")
    public void theFirstCustomerHasMadeValidTransactionToTheMerchant(int numberOfValidTransactions) throws Exception {
        dtuPay.requestTokens(customer.getUserID(), 2);
        BigDecimal amountBD = new BigDecimal(50);
        for (int i = 0; i < numberOfValidTransactions; i++) {
            //TODO: Change after refactoring
            dtuPay.scanToken(merchant.getUserID(), customer.getUserID());
            transactionRequest = transactionManager.generateTransaction(merchant.getCustomerToken(), merchant, amountBD, customer, "description");

            transactionManager.processTransaction(transactionRequest);
            ITokenManager.removeToken(customer.getNextToken().getTokenID());
            customer.consumeToken();

        }

    }

    @And("^The second customer has made (\\d+) valid transaction to the merchant$")
    public void theSecondCustomerHasMadeValidTransactionToTheMerchant(int numberOfValidTransactions) throws Exception {
        dtuPay.requestTokens(secondCustomer.getUserID(), 2);
        BigDecimal amountBD = new BigDecimal(50);
        for (int i = 0; i < numberOfValidTransactions; i++) {
            //TODO: Change after refactoring
            dtuPay.scanToken(merchant.getUserID(), secondCustomer.getUserID());
            secondCustomer.consumeToken();
            transactionRequest = transactionManager.generateTransaction(merchant.getCustomerToken(), merchant, amountBD, secondCustomer, "description");
            transactionManager.processTransaction(transactionRequest);
            ITokenManager.removeToken(secondCustomer.getNextToken().getTokenID());

        }
    }

    @When("^The merchant requests a report of all transactions$")
    public void theMerchantRequestsAReportOfAllTransactions() throws Exception {
        report = dtuPay.getReportByMerchantID(merchant.getUserID().toString());
    }

    @When("^The customer wants a report of all the customers transactions$")
    public void theCustomerWantsAReportOfAllTheCustomersTransactions() throws Exception {
        report = dtuPay.getReportByCustomerID(customer.getUserID().toString());
    }

    @Then("^A list of (\\d+) transactions is received$")
    public void aListOfTransactionsIsReceived(int numberOfTransactions) {
        Assert.assertEquals(numberOfTransactions, report.size());
    }

}*/
