package com.dtupay.monolith;



import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
/*
public class TokenSteps {

    private Customer customer;
    private ITokenManager ITokenManager;
    private ICustomerRepository customerRepository;
    private DTUPay dtuPay;

    public TokenSteps(Helper helper) {
        this.ITokenManager = helper.getTokenManager();
        this.dtuPay = helper.getDtuPay();
    }

    // ###### Request Tokens ######
    // ############################
    @Given("^I have (\\d+) tokens$")
    public void iHaveTokens(int initial) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        customer = dtuPay.registerCustomer("1234","Customer","Customer", "123456789");
        dtuPay.requestTokens(customer.getUserID(),initial);
    }

    @When("^I request (\\d+) tokens$")
    public void iRequestTokens(int requested) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        dtuPay.requestTokens(customer.getUserID(),requested);
    }

    @Then("^I now have (\\d+) tokens$")
    public void iNowHaveFinalTokens(int finalTokenAmount) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        assertEquals(finalTokenAmount, customer.numberOfTokens());
    }
    // ###### Use Tokens ######
    // ########################
    @Given("^I have (\\d+) unused token$")
    public void iHaveUnusedToken(int numberOfUnusedTokens) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        customer = dtuPay.registerCustomer("4567","Customer","Customer", "123456789");
        dtuPay.requestTokens(customer.getUserID(),numberOfUnusedTokens);
        assertFalse(ITokenManager.isTokenUsed(customer.getNextToken().getTokenID()));
    }

    @When("^I use a token$")
    public void iUseAToken() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        dtuPay.useToken(customer.getUserID());
    }

    @Then("^The token is accepted$")
    public void theTokenIsAccepted() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        assertTrue(dtuPay.getTokenStatus(customer.getNextToken().getTokenID()));;
    }

    // ###### Already Used Tokens ######
    // #################################
    @Given("^I have (\\d+) used token$")
    public void iHaveUsedToken(int numberOfTokens) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        customer = dtuPay.registerCustomer("0923","Customer","Customer", "123456789");
        dtuPay.requestTokens(customer.getUserID(),numberOfTokens);
        ITokenManager.useToken(customer.getNextToken());
        assertTrue(ITokenManager.isTokenUsed(customer.getNextToken().getTokenID()));
    }

    @Then("^the token is rejected$")
    public void theTokenIsRejected() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        assertFalse(dtuPay.getTokenStatus(customer.getNextToken().getTokenID()));;
    }

    // ###### Fake Token ######
    // ############################
    @Given("^I have (\\d+) fake token$")
    public void iHaveFakeToken(int arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        customer = dtuPay.registerCustomer("1002","Customer","Customer", "123456789");
        customer.addTokens(Collections.singletonList(new Token("fakeToken")));
    }
}*/

