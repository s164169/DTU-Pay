package com.dtupay.monolith;

import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtu.ws.fastmoney.BankServiceException_Exception;

import java.io.InvalidObjectException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
/*
public class TransactionSteps {

    private Customer customer;
    private ITokenManager ITokenManager;
    private Merchant merchant;
    private TransactionManager transactionManager;
    private TransactionRequest transactionRequest;
    private DTUPay dtuPay;
    private List<String> accounts = new ArrayList<>();

    public TransactionSteps(Helper helper) {
        this.ITokenManager = helper.getTokenManager();
        //this.transactionManager = helper.getTransactionManager();  --- til mono
        //this.dtuPay = helper.getDtuPay();                          --- til mono
        this.transactionManager = helper.getTransactionManagerSoap();
        this.dtuPay = helper.getDtuPaySoap();

    }

    @After
    public void cleanupUsedAccounts() throws Exception {
        for (String account : accounts) {
            dtuPay.retireAccount(account);
        }
    }

    @Given("^A merchant scans (\\d+) unused customer token$")
    public void aMerchantScansUnusedCustomerToken(int numberOfTokens) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        dtuPay.requestTokens(customer.getUserID(),numberOfTokens);
        dtuPay.scanToken(merchant.getUserID(),customer.getUserID());
    }

    @When("^The merchant requests payment of (\\d+) DKK with the customer's token$")
    public void theMerchantRequestsPaymentOfDKKWithTheCustomerSToken(int amount) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        BigDecimal amountBD = new BigDecimal(amount);
        transactionRequest = dtuPay.generateTransaction(merchant.getUserID(), customer.getUserID(), amountBD, "TestPayment");
        try {
            transactionManager.processTransaction(transactionRequest);
        } catch (InvalidObjectException e) {
            //TODO: Ask how we should assert a correct exception / error
            //e.printStackTrace(); uncomment to see invalid token trace
        }
    }

    @Then("^The payment succeeds$")
    public void thePaymentSucceeds() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        assertTrue(transactionManager.isTransactionSucceeded(transactionRequest.getTransactionRequestID().toString()));
    }

    @Given("^A merchant scans (\\d+) used customer token$")
    public void aMerchantScansUsedCustomerToken(int numberOfTokens) throws Throwable {
        //customer.addTokens(tokenManager.requestTokens(customer.getUserID(), numberOfTokens));
        dtuPay.requestTokens(customer.getUserID(),numberOfTokens);
        // Simulate the act of the token being already used
        // This does violate dependency inversion principles but is required for tests to work without writing code that creates fake transactions
        ITokenManager.useToken(customer.getNextToken());
        dtuPay.scanToken(merchant.getUserID(),customer.getUserID());
        assertTrue(dtuPay.getTokenStatus(merchant.getCustomerToken().getTokenID()));
    }

    @Then("^The payment does not succeed$")
    public void thePaymentDoesNotSucceed() throws Throwable {
        assertFalse(dtuPay.getTransactionBoolean(transactionRequest));
    }

    @Given("^A merchant scans (\\d+) fake customer token$")
    public void aMerchantScansFakeCustomerToken(int arg0) throws Throwable {
        // Required to create a fake token
        for (int i = 0; i < arg0; i++) {
            customer.addTokens(Collections.singletonList(new Token("fakeToken")));
        }
        dtuPay.scanToken(merchant.getUserID(),customer.getUserID());

    }

    @Given("^A merchant has a bank account$")
    public void aMerchantHasABankAccount() throws Throwable {
        this.merchant = dtuPay.registerMerchant("475","xx1","zz1", "123456789");
        //Used for account cleanup
        accounts.add(merchant.getAccountID());
    }

    @And("^A customer has a bank account$")
    public void aCustomerHasABankAccount() throws Throwable {
        this.customer = dtuPay.registerCustomer("476","yy2","zy2", "123456789");
        //Used for account cleanup
        accounts.add(customer.getAccountID());
    }

}
*/