#!/bin/bash
set -e
mvn clean package
# Build the docker image using
docker build --tag rest-server .
# Do a garbage collection of images not used anymore.
docker image prune -f
