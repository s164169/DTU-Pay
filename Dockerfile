FROM adoptopenjdk:8-jre-hotspot
WORKDIR /usr/src
COPY target/restful-endpoint-thorntail.jar /usr/src/target/
COPY src/main/resources/BankService.wsdl /usr/src/src/main/resources/
CMD java -ea -Djava.net.preferIPv4Stack=true -Djava.net.preferIPv4Addresses=true -jar target/restful-endpoint-thorntail.jar
